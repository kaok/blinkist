# blinkist-ab

The application is built using VueJS. The idea was to separate the logic as much as possible to make sure it's easy to understand and maintain.

Since some data is required to be available in individual components, I used VueX as a storage solution. Specifically, I used VueX to store data about a session, ab test results and general tracking data.

I implemented an AB component that can be used to add testing variants to any other component's template. The AB component allows testing multiple components independently.

For the tracking, I created a tracking.js util that is responsible for outputting the pageview and signup events to a console. Of course for production, this has to be changed to use API calls to store the tracking data in DB. But that's a good approach to start from.

Also, I created a simple dashboard to show the results of tests right after the blog post on a page. The dashboard shows the current Session ID, total page views and signups and shows impressions and CTRs of each variant.
The dashboard was the most complicated part to develop because of a need to process data, but in the end, it's quite straightforward.

I hope you will enjoy checking the app!

The built app can be seen here https://silly-noether-cd2367.netlify.app/

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
