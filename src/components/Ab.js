import Cookie from 'js-cookie';

export default {
	props: {
		name: {
			type: String,
			required: true,
		},
		tag: {
			type: String,
			default: "div",
		},
		weight: Object,
		cookieName: {
			type: String,
			default: "ab-test",
		},
		cookieExpires: {
			type: Number,
			default: 7,
		},
		cookiePath: null,
	},
	mounted() {
		this.$store.dispatch('ab/addVariant', { name: this.name, variant: this.variant });
	},
	computed: {
		session() {
			return this.$store.state.session;
		},
		fullCookieName() {
			return `${this.name}_${this.cookieName}`;
		},
		variant() {
			const restoredVariant = Cookie.get(this.fullCookieName);

			if (!this.session.isNew && restoredVariant && restoredVariant in this.$slots) {
				return restoredVariant;
			} else {
				return this.chooseVariant();
			}
		},
	},
	render(h) {
		if (!this.variant) {
			return;
		}

		const variantSlot = this.$slots[this.variant];

		if (variantSlot.length == 1 && variantSlot[0].tag) {
			return variantSlot[0];
		}

		return h(this.tag, variantSlot);
	},
	methods: {
		chooseVariant() {
			const keys = Object.keys(this.$slots);
			let thresholds = [];
			let totalWeights = 0;
			let variant = null;

			for (let i = 0; i < keys.length; i++) {
				let key = keys[i];
				let currWeight = this.weight[key];

				if (!currWeight) {
					continue;
				}

				thresholds.push([key, totalWeights + currWeight]);
				totalWeights += currWeight;
			}

			if (totalWeights) {
				var threshold = Math.random() * totalWeights;

				for (let i = 0; i < thresholds.length; i++) {
					if (thresholds[i][1] > threshold) {
						variant = thresholds[i][0];
						break;
					}
				}
			}

			if (!variant) {
				variant = keys[Math.floor(Math.random() * keys.length)];
			}

			Cookie.set(this.fullCookieName, variant, { expires: this.cookieExpires });

			return variant;
		},
	},
}

