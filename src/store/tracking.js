import Cookie from 'js-cookie';

const totalPageViewsCookie = 'total_page_views';
const totalSignUpsCookie = 'total_signups';
let totalPageViews = Cookie.get(totalPageViewsCookie) || 0;
let totalSignUps = Cookie.get(totalSignUpsCookie) || 0;

const state = {
	totalPageViews,
	totalSignUps,
};

const mutations = {
	incrementPageViews: state => {
		state.totalPageViews++;

		Cookie.set(totalPageViewsCookie, state.totalPageViews);
	},
	incrementSignUps: state => {
		state.totalSignUps++;

		Cookie.set(totalSignUpsCookie, state.totalSignUps);
	},
}


const trackingModule = {
	namespaced: true,
	state,
	mutations,
};

export default trackingModule;