import Cookie from 'js-cookie';

let id = Cookie.get('session_id');

const state = {
	id,
	isNew: false,
};


if (!id) {
	// Generate random 4-digits session Id
	id = Math.floor(1000 + Math.random() * 9000);
	Cookie.set('session_id', id);
	state.id = id;
	state.isNew = true;
}

const sessionModule = {
	namespaced: true,
	state
};

export default sessionModule;