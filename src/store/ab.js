import Cookie from 'js-cookie';

const abResultsCookieKey = 'ab_test_results';
let results = Cookie.get(abResultsCookieKey);

if (typeof results === 'string') {
	results = JSON.parse(results);
}

if (typeof results !== 'object') {
	results = {};
}

const abLastResultsCookieKey = 'ab_test_last_results';
let lastResults = Cookie.get(abLastResultsCookieKey);

if (typeof lastResults === 'string') {
	lastResults = JSON.parse(lastResults);
}

if (typeof lastResults !== 'object') {
	lastResults = {};
}

const abWinnersResultsCookieKey = 'ab_test_winners_results';
let winnersResults = Cookie.get(abWinnersResultsCookieKey);

if (typeof winnersResults === 'string') {
	winnersResults = JSON.parse(winnersResults);
}

if (typeof winnersResults !== 'object') {
	winnersResults = {};
}

const incrementMetricValue = (state, testName, metricName) => {
	Object.keys(state.lastResults).forEach(name => {
		const variantName = state.lastResults[name];

		if (!state.winnersResults[name]) {
			state.winnersResults[name] = {};
		}

		if (!state.winnersResults[name][variantName]) {
			state.winnersResults[name][variantName] = {};
		}

		if (!state.winnersResults[name][variantName].pageviews) {
			state.winnersResults[name][variantName].pageviews = 0;
		}

		if (!state.winnersResults[name][variantName].signups) {
			state.winnersResults[name][variantName].signups = 0;
		}
	});

	Object.keys(state.lastResults).forEach(name => {
		if (name !== testName) {
			return;
		}

		const variantName = state.lastResults[name];

		state.winnersResults[name][variantName][metricName] = parseInt(state.winnersResults[name][variantName][metricName]) + 1;
	});

}

const updateVariantPageView = (state, rootState, testName) => {
	if (!rootState.session.isNew || !Object.keys(state.lastResults).length) {
		return;
	}

	incrementMetricValue(state, testName, 'pageviews');
	Cookie.set(abWinnersResultsCookieKey, state.winnersResults);
}

const state = {
	results,
	lastResults,
	winnersResults,
};

const actions = {
	addVariant({ state, rootState, commit }, payload) {
		if (!rootState.session.isNew) {
			return;
		}

		if (!state.results[rootState.session.id]) {
			state.results[rootState.session.id] = {};
		}

		commit('addVariant', {
			sessionId: rootState.session.id,
			name: payload.name,
			variant: payload.variant,
		})

		state.lastResults[payload.name] = payload.variant;

		Cookie.set(abResultsCookieKey, state.results);
		Cookie.set(abLastResultsCookieKey, state.lastResults);

		updateVariantPageView(state, rootState, payload.name);
	},
	updateVariantSignUps({ state }) {
		if (!Object.keys(state.lastResults).length) {
			return;
		}

		Object.keys(state.lastResults).forEach(testName => {
			incrementMetricValue(state, testName, 'signups');
		});

		Cookie.set(abWinnersResultsCookieKey, state.winnersResults);
	},
}

const mutations = {
	addVariant(state, payload) {
		state.results = {
			...state.results,
			[payload.sessionId]: { ...state.results[payload.sessionId], [payload.name]: payload.variant }
		}
	}
}


const abModule = {
	namespaced: true,
	state,
	actions,
	mutations,
};

export default abModule;