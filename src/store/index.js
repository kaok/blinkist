import Vue from 'vue';
import Vuex from 'vuex';
import abModule from './ab'
import sessionModule from './session'
import trackingModule from './tracking'

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		session: sessionModule,
		ab: abModule,
		tracking: trackingModule,
	},
})

export default store;