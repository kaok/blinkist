import Cookie from 'js-cookie';

export const track = payload => {
	payload.url = window.location.href;
	console.log(`--> Event ${payload.name} URL: ${window.location.href}`);
};

export const trackOnce = (payload, sessionId) => {
	const eventCookieName = `once_event_${payload.name}_${sessionId}`;
	const currentValue = Cookie.get(eventCookieName);

	if (currentValue) {
		return;
	}

	Cookie.set(eventCookieName, true);

	track(payload);
};

export const trackNewSessionPageview = (sessionId) => {
	trackOnce({ name: 'Pageview' }, sessionId)
}

export const eventTrackedOnce = (payload, sessionId) => {
	const eventCookieName = `once_event_${payload.name}_${sessionId}`;
	return !!Cookie.get(eventCookieName);
}